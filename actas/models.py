from django.db import models

# Create your models here.
class Eleccion(models.Model):
    descripcion = models.CharField(max_length=255)

    def __str__(self):
        return self.descripcion

class Municipio(models.Model):
    descripcion = models.CharField(max_length=255)

    def __str__(self):
        return self.descripcion

class Distrito(models.Model):
    descripcion = models.CharField(max_length=255)

    def __str__(self):
        return self.descripcion

class Seccion(models.Model):
    descripcion = models.CharField(max_length=255)

    def __str__(self):
        return self.descripcion

class TipoCasilla(models.Model):
    descripcion = models.CharField(max_length=255)

    def __str__(self):
        return self.descripcion

    class Meta:
        ordering = ['descripcion']

class Casilla(models.Model):
    seccion = models.ForeignKey(Seccion, on_delete=models.RESTRICT)
    tipo_casilla = models.ForeignKey(TipoCasilla, on_delete=models.RESTRICT)

    def __str__(self):
        return '%s - %s ' % (self.seccion, self.tipo_casilla)

class TipoActa(models.Model):
    descripcion = models.CharField(max_length=255)

    def __str__(self):
        return self.descripcion