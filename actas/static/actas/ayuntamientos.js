$(document).ready(function(){

    $('#div_id_distrito').hide();
    $('#div_id_municipio').hide();
    $('#no-archive').hide();

    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            get_tipos()
            return false;
        }
    });

    $('#id_tipo_acta').on('change', function(){
        id_tipo_acta = parseInt($('#id_tipo_acta').val())
        switch (id_tipo_acta){
            // CASILLAS Y PUNTOS DE RECUENTO
            case 1:
            case 2:
                $('#div_id_seccion').show();
                $('#div_id_tipo_casilla').show();
                $('#div_id_distrito').hide();
                $('#div_id_municipio').hide();
                break;
            // CONSEJO GENERAL
            case 3:
                $('#div_id_seccion').hide();
                $('#div_id_tipo_casilla').hide();
                $('#div_id_distrito').hide();
                $('#div_id_municipio').show();
                get_municipios(0)
                break;
            // DISTRITOS
            case 4:
                $('#div_id_seccion').hide();
                $('#div_id_tipo_casilla').hide();
                $('#div_id_distrito').show();
                $('#div_id_municipio').show();
                break;
        }
        
        get_acta()
    });

    $('#id_seccion').on('change', function(){
        get_tipos()
        $('#acta_frame').attr('src', ''); 
    });

    $('#id_tipo_casilla').on('change', function(){
        get_acta()
    });

    $('#id_distrito').on('change', function(){
        id_distrito = $('#id_distrito').val()

        if(['6','14','15'].indexOf(id_distrito) > -1){
            //Cargar Municipios
            get_municipios($('#id_distrito').val())
            $('#div_id_municipio').show()
            get_acta()
        } else {
            $('#div_id_municipio').hide()
            get_acta()
        }
        
    });

    $('#id_municipio').on('change', function(){
        get_acta()
    });

    function get_municipios(distrito) {
        $('#id_municipio').empty()

        $.ajax({
            type: "POST",
            url: '../municipios/',
            data: {
                distrito : distrito
            },
            success: function(data)
            {
                $('#id_municipio').append($('<option>', {
                    value: 0,
                    text: '<SELECCIONAR>'
                }));

                municipios = JSON.parse(data['municipios'])
                for (var i in municipios){
                    municipio = municipios[i]
                    $('#id_municipio').append($('<option>', {
                        value: municipio.pk,
                        text: municipio.fields.descripcion
                    }));
                }
            }
        });
    }

    function get_tipos() {
        $('#id_tipo_casilla').empty()

        $.ajax({
            type: "POST",
            url: '../tipos-casilla/',
            data: {
                seccion : $('#id_seccion').val()
            },
            success: function(data)
            {
                $('#id_tipo_casilla').append($('<option>', {
                    value: 0,
                    text: '<SELECCIONAR>'
                }));

                tipos_casillas = JSON.parse(data['tipos_casillas'])
                for (var i in tipos_casillas){
                    tipo_casilla = tipos_casillas[i]
                    $('#id_tipo_casilla').append($('<option>', {
                        value: tipo_casilla.pk,
                        text: tipo_casilla.fields.descripcion
                    }));
                }
            }
        });
    }

    function get_acta() {
        $.ajax({
            type: "POST",
            url: 'acta/',
            data: {
                tipo_acta : $('#id_tipo_acta').val(),
                seccion : $('#id_seccion').val(),
                tipo_casilla : $('#id_tipo_casilla').val(),
                distrito : $('#id_distrito').val(),
                municipio : $('#id_municipio').val()
            },
            success: function(data)
            {
                if (data['archivo'] == ''){
                    $('#no-archive').show();
                } else {
                    $('#no-archive').hide();  
                }
                
                $('#acta_frame').attr('src', data['archivo']);  
            }
        });
    }
});