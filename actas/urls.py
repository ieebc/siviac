from django.urls import path

from django.contrib.auth.views import LoginView, LogoutView

from actas import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    #HOME
    path('', views.home, name='home'),
    path('diputaciones/', views.diputaciones, name='diputaciones'),
    path('diputaciones/acta/', views.diputaciones_acta, name='diputaciones-acta'),

    path('ayuntamientos/', views.ayuntamientos, name='ayuntamientos'),
    path('ayuntamientos/acta/', views.ayuntamientos_acta, name='ayuntamientos-acta'),

    path('gubernatura/', views.gubernatura, name='gubernatura'),
    path('gubernatura/acta/', views.gubernatura_acta, name='gubernatura-acta'),

    path('tipos-casilla/', views.tipos_casilla, name='tipos-casilla'),
    path('municipios/', views.municipios, name='municipios'),
    
    path('scripts/', views.scripts, name='scripts'),
    path('scripts/tipos/', views.scripts_tipos, name='scripts-tipos'),
    path('scripts/secciones/', views.scripts_secciones, name='scripts-secciones'),
    path('scripts/casillas/', views.scripts_casillas, name='scripts-casillas'),

    #AUTH
    path('login/', LoginView.as_view(template_name='actas/login.html', redirect_authenticated_user=True), name='login'),
    path('logout/', LogoutView.as_view(template_name='actas/logout.html'), name='logout'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)