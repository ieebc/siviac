from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Eleccion)
admin.site.register(Municipio)
admin.site.register(Distrito)
admin.site.register(Seccion)
admin.site.register(TipoCasilla)
admin.site.register(TipoActa)