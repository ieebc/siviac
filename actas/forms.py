from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field
from .models import *

class_form_select = {'class':'form-control form-select'}
class_form_select_sm = {'class':'form-control form-select-sm'}

tipos_acta_diputaciones = ['Casillas', 'Punto de Recuento', 'MR', 'RP']
tipos_acta_ayuntamientos = ['Casillas', 'Punto de Recuento', 'Consejo General', 'Distritos']


class DiputacionesForm(forms.Form):
    tipo_acta = forms.ModelChoiceField(label='Tipo Acta',queryset=TipoActa.objects.filter(descripcion__in = tipos_acta_diputaciones),widget=forms.Select(attrs=class_form_select_sm), initial=0)
    seccion = forms.IntegerField(label='No. Sección', required=False, widget=forms.NumberInput(attrs=class_form_select_sm))
    tipo_casilla = forms.ModelChoiceField(label='Tipo Casilla', required=False,queryset=TipoCasilla.objects.none(),widget=forms.Select(attrs=class_form_select_sm), empty_label="<SELECCIONAR>")
    distrito = forms.ModelChoiceField(label='Distrito',queryset=Distrito.objects.all(),widget=forms.Select(attrs=class_form_select_sm), empty_label="<SELECCIONAR>")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(
                Field('tipo_acta', wrapper_class="col-3"),
                Field('seccion', wrapper_class="col"),
                Field('tipo_casilla', wrapper_class="col"),
                Field('distrito', wrapper_class="col"),
            css_class='row'),
        )


class AyuntamientosForm(forms.Form):
    tipo_acta = forms.ModelChoiceField(label='Tipo Acta',queryset=TipoActa.objects.filter(descripcion__in = tipos_acta_ayuntamientos),widget=forms.Select(attrs=class_form_select_sm), initial=0, empty_label="<SELECCIONAR>")
    seccion = forms.IntegerField(label='No. Sección', required=False, widget=forms.NumberInput(attrs=class_form_select_sm))
    tipo_casilla = forms.ModelChoiceField(label='Tipo Casilla', required=False,queryset=TipoCasilla.objects.none(),widget=forms.Select(attrs=class_form_select_sm), initial=0, empty_label="<SELECCIONAR>")
    distrito = forms.ModelChoiceField(label='Distrito',required=False,queryset=Distrito.objects.all(),widget=forms.Select(attrs=class_form_select_sm), initial=0, empty_label="<SELECCIONAR>")
    municipio = forms.ModelChoiceField(label='Municipio',required=False,queryset=Municipio.objects.all(),widget=forms.Select(attrs=class_form_select_sm), initial=0, empty_label="<SELECCIONAR>")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(
                Field('tipo_acta', wrapper_class="col-3"),
                Field('seccion', wrapper_class="col"),
                Field('tipo_casilla', wrapper_class="col"),
                Field('distrito', wrapper_class="col"),
                Field('municipio', wrapper_class="col"),
            css_class='row'),
        )

class GubernaturaForm(forms.Form):
    tipo_acta = forms.ModelChoiceField(label='Tipo Acta',queryset=TipoActa.objects.filter(descripcion__in = tipos_acta_ayuntamientos),widget=forms.Select(attrs=class_form_select_sm), initial=0, empty_label="<SELECCIONAR>")
    seccion = forms.IntegerField(label='No. Sección', required=False, widget=forms.NumberInput(attrs=class_form_select_sm))
    tipo_casilla = forms.ModelChoiceField(label='Tipo Casilla', required=False,queryset=TipoCasilla.objects.none(),widget=forms.Select(attrs=class_form_select_sm), initial=0, empty_label="<SELECCIONAR>")
    distrito = forms.ModelChoiceField(label='Distrito',required=False,queryset=Distrito.objects.all(),widget=forms.Select(attrs=class_form_select_sm), initial=0, empty_label="<SELECCIONAR>")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(
                Field('tipo_acta', wrapper_class="col-3"),
                Field('seccion', wrapper_class="col"),
                Field('tipo_casilla', wrapper_class="col"),
                Field('distrito', wrapper_class="col"),
            css_class='row'),
        )