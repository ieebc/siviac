from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers import serialize
from os.path import exists
from django.templatetags.static import static
from django.contrib.staticfiles import finders
from django.conf import settings

from .forms import *

import pyodbc
import json
import os

TIPO_ACTA_FOLDER = {
    1: 'ACTAS CASILLAS',
    2: 'ACTAS PUNTO DE RECUENTO',
    3: 'CONSEJO GENERAL',
    4: 'DISTRITOS',
    5: 'MR',
    6: 'RP'
}

MUNICIPIOS_X_DISTRITO = {
    '6': ['Ensenada', 'Tecate', 'Tijuana'],
    '14': ['Tecate', 'Tijuana'],
    '15': ['Ensenada', 'Mexicali', 'Playas de Rosarito']
}

# Create your views here.
@login_required
def home(request):
    return redirect('diputaciones')

@login_required
def diputaciones(request):
    archivo = 'actas/test.pdf'
    if request.method == 'POST':
        form = DiputacionesForm(request.POST)
    else:
        form = DiputacionesForm()
    
    context = { 'url_name': 'diputaciones', 'form': form, 'archivo': archivo}
    return render(request, 'actas/diputaciones.html', context)

@csrf_exempt
def diputaciones_acta(request):
    tipo_acta = int(request.POST['tipo_acta'])
    archivo = 'actas/archivos/DIPUTACIONES/%s/' % (TIPO_ACTA_FOLDER[tipo_acta])
    
    if tipo_acta in [1,2]:
        if request.POST['seccion'] and request.POST['tipo_casilla'] != '0':
            seccion = request.POST['seccion'].lstrip("0")
            tipo_casilla = request.POST['tipo_casilla']
            tipo_casilla = TipoCasilla.objects.filter(id = tipo_casilla).first()
            archivo += '%s %s.pdf' % (seccion.zfill(4), tipo_casilla.descripcion)
        else:
            archivo += '.pdf'
    if tipo_acta in [5,6]:
        if request.POST['distrito']:
            distrito = Distrito.objects.filter(id = request.POST['distrito']).first()
            archivo += distrito.descripcion + '.pdf'
        else:
            archivo += '.pdf'

    busqueda = settings.STATIC_ROOT + archivo
    if not os.path.exists(busqueda):
        archivo = ''
    else:
        archivo = static(archivo)
    
    return JsonResponse({"archivo": archivo}, status=200)

@login_required
def ayuntamientos(request):
    archivo = 'actas/test.pdf'
    if request.method == 'POST':
        form = AyuntamientosForm(request.POST)
    else:
        form = AyuntamientosForm()
    
    context = { 'url_name': 'ayuntamientos', 'form': form, 'archivo': archivo}
    return render(request, 'actas/ayuntamientos.html', context)

@csrf_exempt
def ayuntamientos_acta(request):
    tipo_acta = int(request.POST['tipo_acta'])
    archivo = 'actas/archivos/AYUNTAMIENTOS/%s/' % (TIPO_ACTA_FOLDER[tipo_acta])
    
    if tipo_acta in [1,2]:
        if request.POST['seccion'] and request.POST['tipo_casilla'] != '0':
            seccion = request.POST['seccion'].lstrip("0")
            tipo_casilla = request.POST['tipo_casilla']
            tipo_casilla = TipoCasilla.objects.filter(id = tipo_casilla).first()
            archivo += '%s %s.pdf' % (seccion.zfill(4), tipo_casilla.descripcion)
        else:
            archivo += '.pdf'
    if tipo_acta == 3:
        if request.POST['municipio']:
            municipio = request.POST['municipio']
            municipio = Municipio.objects.filter(id = municipio).first()
            if municipio.descripcion == 'Playas de Rosarito':
                archivo += 'Acta %s.pdf' % 'Rosarito'
            else:
                archivo += 'Acta %s.pdf' % municipio.descripcion
        else:
            archivo += '.pdf'
    if tipo_acta == 4:
        distrito = request.POST['distrito']
        if request.POST['distrito']:
            if distrito in ['6', '14', '15']:
                if request.POST['municipio']:
                    municipio = request.POST['municipio']
                    municipio = Municipio.objects.filter(id = municipio).first()
                    distrito = Distrito.objects.filter(id = distrito).first()
                    if municipio.descripcion == 'Playas de Rosarito':
                        archivo += '%s %s.pdf' % (distrito.descripcion, 'Rosarito')
                    else:
                        archivo += '%s %s.pdf' % (distrito.descripcion, municipio.descripcion)
                    
                else:
                    archivo += '.pdf'
            else:
                distrito = Distrito.objects.filter(id = distrito).first()
                archivo += '%s.pdf' % distrito.descripcion
        else:
            archivo += '.pdf'

    busqueda = settings.STATIC_ROOT + archivo
    if not os.path.exists(busqueda):
        archivo = ''
    else:
        archivo = static(archivo)
    
    return JsonResponse({"archivo": archivo}, status=200)

@login_required
def gubernatura(request):
    archivo = 'actas/test.pdf'
    if request.method == 'POST':
        form = GubernaturaForm(request.POST)
    else:
        form = GubernaturaForm()
    
    context = { 'url_name': 'gubernatura', 'form': form, 'archivo': archivo}
    return render(request, 'actas/gubernatura.html', context)

@csrf_exempt
def gubernatura_acta(request):
    tipo_acta = int(request.POST['tipo_acta'])
    archivo = 'actas/archivos/GUBERNATURA/%s/' % (TIPO_ACTA_FOLDER[tipo_acta])
    
    if tipo_acta in [1,2]:
        if request.POST['seccion'] and request.POST['tipo_casilla'] != '0':
            seccion = request.POST['seccion'].lstrip("0")
            tipo_casilla = request.POST['tipo_casilla']
            tipo_casilla = TipoCasilla.objects.filter(id = tipo_casilla).first()
            archivo += '%s %s.pdf' % (seccion.zfill(4), tipo_casilla.descripcion)
        else:
            archivo += '.pdf'
    if tipo_acta == 3:
        archivo += 'Acta Gubernatura CG.pdf'
    if tipo_acta == 4:
        if request.POST['distrito']:
            distrito = request.POST['distrito']
            distrito = Distrito.objects.filter(id = distrito).first()
            archivo += '%s.pdf' % distrito.descripcion
        else:
            archivo += '.pdf'

    busqueda = settings.STATIC_ROOT + archivo
    if not os.path.exists(busqueda):
        archivo = ''
    else:
        archivo = static(archivo)
    
    return JsonResponse({"archivo": archivo}, status=200)

@csrf_exempt
def tipos_casilla(request):
    seccion = request.POST['seccion'].lstrip("0")
    casillas_set = Casilla.objects.filter(seccion__descripcion = seccion).values_list('tipo_casilla')
    tipos_casillas = TipoCasilla.objects.filter(id__in = casillas_set)
    data = serialize("json", tipos_casillas)
    return JsonResponse({"tipos_casillas": data}, status=200)

@csrf_exempt
def municipios(request):    
    distrito = request.POST['distrito']
    if distrito in ['6', '14', '15']:
        print(distrito)
        municipios = Municipio.objects.filter(descripcion__in = MUNICIPIOS_X_DISTRITO[distrito])
    else:
        municipios = Municipio.objects.all()
    data = serialize("json", municipios)
    return JsonResponse({"municipios": data}, status=200)

#SCRIPTS
@login_required
def scripts(request):
    context = { 'url_name': 'scripts'}
    return render(request, 'actas/scripts.html', context)

def scripts_tipos(request):
    conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.254.136;PORT=1433;DATABASE=JornadaCanto2021;UID=acceso;PWD=dell2016;')
    cursor = conn.cursor()
    cursor.execute('SELECT DISTINCT CASILLALARGA FROM Canto_CasillaDip WHERE CASILLALARGA IS NOT NULL')

    for i in cursor:
        tipo = TipoCasilla.objects.filter(descripcion = i[0]).count()
        if tipo == 0:
            tipo = TipoCasilla()
            tipo.descripcion = i[0]
            tipo.save()

    messages.success(request, 'Tipos Sincronizados!')
    return redirect('scripts')

def scripts_secciones(request):
    conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.254.136;PORT=1433;DATABASE=JornadaCanto2021;UID=acceso;PWD=dell2016;')
    cursor = conn.cursor()
    cursor.execute('SELECT DISTINCT SECCION FROM Canto_CasillaDip WHERE SECCION IS NOT NULL')

    for i in cursor:
        seccion = Seccion.objects.filter(descripcion = i[0]).count()
        if seccion == 0:
            seccion = Seccion()
            seccion.descripcion = i[0]
            seccion.save()

    messages.success(request, 'Secciones Sincronizadas!')
    return redirect('scripts')

def scripts_casillas(request):
    conn = pyodbc.connect('DRIVER={SQL Server};SERVER=192.168.254.136;PORT=1433;DATABASE=JornadaCanto2021;UID=acceso;PWD=dell2016;')
    cursor = conn.cursor()
    cursor.execute('SELECT SECCION, CASILLALARGA FROM Canto_CasillaDip WHERE SECCION IS NOT NULL')

    for i in cursor:
        seccion = Seccion.objects.filter(descripcion = i[0]).first()
        tipo_casilla = TipoCasilla.objects.filter(descripcion = i[1]).first()
        if seccion and tipo_casilla:
            casilla = Casilla.objects.filter(seccion = seccion, tipo_casilla = tipo_casilla).first()
            if not casilla:
                casilla = Casilla()
                casilla.seccion = seccion
                casilla.tipo_casilla = tipo_casilla
                casilla.save()

    messages.success(request, 'Casillas Sincronizadas!')
    return redirect('scripts')